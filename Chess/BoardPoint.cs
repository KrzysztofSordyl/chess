﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public struct BoardPoint
    {
        public static BoardPoint Parse(string s)
        {
            // TODO
            throw new NotImplementedException();
        }

        public BoardPoint(int rank, int file)
        {
            this.Rank = rank;
            this.File = file;
        }

        public int Rank { get; }

        public int File { get; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append((char)(this.File + 'A' - 1));
            sb.Append(this.Rank);
            return sb.ToString();
        }

        public static BoardPoint operator -(BoardPoint left, BoardPoint right)
        {
            return new BoardPoint(left.Rank - right.Rank, left.File - right.File);
        }
    }
}
