﻿using Chess.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class InPassing : IDisposable
    {
        public InPassing(Field attacked, Field rightWing, Field leftWing, Piece piece)
        {
            this.Attacked = attacked;
            this.RightWing = rightWing;
            this.LeftWing = leftWing;
            this.Piece = piece;

            (this.RightWing?.Piece as Pawn)?.EnableInPassing(this.Attacked, piece);
            (this.LeftWing?.Piece as Pawn)?.EnableInPassing(this.Attacked, piece);
        }

        public Field Attacked { get; }
        public Field RightWing { get; }
        public Field LeftWing { get; }
        public Piece Piece { get; }

        public void Dispose()
        {
            (this.RightWing?.Piece as Pawn)?.DisableInPassing(this.Attacked, this.Piece);
            (this.LeftWing?.Piece as Pawn)?.DisableInPassing(this.Attacked, this.Piece);
        }
    }
}
