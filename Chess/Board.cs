﻿using Chess.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Board : IDisposable
    {
        public static Board Create()
        {
            Board board = new Board();
            setBackLine(board, Color.White);
            setBackLine(board, Color.Black);
            setFrontLine(board, Color.White);
            setFrontLine(board, Color.Black);
            return board;

            void setBackLine(Board b, Color c)
            {
                int rank = c == Color.White ? 1 : 8;
                b[rank, 1].Piece = new Rook(c);
                b[rank, 2].Piece = new Knight(c);
                b[rank, 3].Piece = new Bishop(c);
                b[rank, 4].Piece = new Queen(c);
                b[rank, 5].Piece = new King(c);
                b[rank, 6].Piece = new Bishop(c);
                b[rank, 7].Piece = new Knight(c);
                b[rank, 8].Piece = new Rook(c);
            }
            void setFrontLine(Board b, Color c)
            {
                int rank = c == Color.White ? 2 : 7;
                for (int file = 1; file <= 8; file++)
                {
                    b[rank, file].Piece = new Pawn(c);
                }
            }
        }

        public static Board Load(ChessMoveStorage game)
        {
            Board board = Board.Create();
            foreach (var move in game.Moves)
            {
                board.MakeMove(move);
            }
            return board;
        }

        private Field[,] fields { get; } = new Field[8, 8];

        public Field this[int rank, int file]
        {
            get => this.fields[rank - 1, file - 1];
            private set => this.fields[rank - 1, file - 1] = value;
        }
        public Field this[BoardPoint point]
        {
            get => this[point.Rank, point.File];
            private set => this[point.Rank, point.File] = value;
        }

        public Board()
        {
            for (int rank = 1; rank <= 8; rank++)
            {
                for (int file = 1; file <= 8; file++)
                {
                    this[rank, file] = new Field(
                        rank,
                        file,
                        this,
                        (rank + file) % 2 == 1
                        ? Color.White
                        : Color.Black,
                        null,
                        rank == 1
                        ? null
                        : this[rank - 1, file],
                        file == 1
                        ? null
                        : this[rank, file - 1],
                        null);
                }
            }
        }
        public (bool Check, bool Mate) MakeMove(Move move)
        {
            Field from = this[move.From];
            Field to = this[move.To];
            if (from.Piece == null)
                throw new InvalidOperationException("Piece does not exist on specified field.");
            Piece piece = from.Piece;
            Piece attacked = to.Piece;
            if (attacked == null)
            {
                Capture m = piece.Moves.FirstOrDefault(c => c.Field == to);
                
                if (this.inPassing != null && 
                    to == this.inPassing.Attacked &&
                    (from == this.inPassing.LeftWing ||
                    from == this.inPassing.RightWing))
                {
                    from.Piece = null;
                    var captured = this.inPassing.Piece;
                    captured.Dispose();
                    var removedFrom = this.inPassing.Piece.Field;
                    this.inPassing.Piece.Field = null;
                    to.Piece = piece;
                    this.AdditionalFieldChanged(removedFrom, null);
                }
                else if (m == null || m.Blockings.Count > 0)
                    throw new InvalidOperationException("Move is not available for this piece.");
                this.inPassing?.Dispose();
                this.inPassing = null;

                if (piece.Type == PieceType.Pawn &&
                    Math.Abs((from.Position - to.Position).Rank) == 2)
                {
                    Field field = null;
                    Field rightWing = to.Right;
                    Field leftWing = to.Left;
                    switch (from.Piece.Color)
                    {
                        case Color.White:
                            field = from.Up;
                            break;
                        case Color.Black:
                            field = from.Down;
                            break;
                    }
                    this.inPassing = new InPassing(field, rightWing, leftWing, piece);
                }
                this.makeMove(from, to, piece);
            }
            else
            {
                this.inPassing?.Dispose();
                this.inPassing = null;
                Capture m = piece.Captures.FirstOrDefault(c => c.Field == to);
                if (m == null || m.Blockings.Count > 0)
                    throw new InvalidOperationException("Attack is not available for this piece.");
                this.makeCapture(from, to, piece);
            }
            return (false, false);
        }

        private void makeMove(Field from, Field to, Piece piece)
        {
            from.Piece = null;
            to.Piece = this.makePromotion(to, piece);
        }
        private void makeCapture(Field from, Field to, Piece piece)
        {
            from.Piece = null;
            var captured = to.Piece;
            captured.Dispose();
            to.Piece = this.makePromotion(to, piece);
        }
        private Piece makePromotion(Field to, Piece piece)
        {
            if (piece.Type == PieceType.Pawn &&
                ((to.Rank == 1 && piece.Color == Color.Black) ||
                (to.Rank == 8 && piece.Color == Color.White)))
            {
                PieceType type = 0;
                switch (piece.Color)
                {
                    case Color.White:
                        type = this.WhitePromotion();
                        break;
                    case Color.Black:
                        type = this.BlackPromotion();
                        break;
                }
                return Piece.Create(type, piece.Color);
            }
            else return piece;
        }
        private InPassing inPassing;

        public event PieceChangedEventHandler AdditionalFieldChanged;

        public event PromotionEventHandler WhitePromotion;
        public event PromotionEventHandler BlackPromotion;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Board() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public delegate PieceType PromotionEventHandler();
}
