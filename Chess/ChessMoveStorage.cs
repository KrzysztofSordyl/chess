﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class ChessMoveStorage
    {
        public List<Move> Moves { get; } = new List<Move>();

        public Color CurrentPlayer { get; set; } = Color.White;
    }
}
