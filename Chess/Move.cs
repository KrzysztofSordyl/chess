﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Move
    {
        public static Move Parse(string s)
        {
            string moveString = s.Replace(" ", "");
            if (moveString.Length != 4)
                return null;

            int fromRank;
            if (!int.TryParse(moveString.Substring(1, 1), out fromRank))
                return null;
            int toRank;
            if (!int.TryParse(moveString.Substring(3, 1), out toRank))
                return null;
            char fromFile = moveString[0];
            char toFile = moveString[2];

            if (fromRank < 1 || fromRank > 8 ||
                toRank < 1 || toRank > 8 ||
                fromFile < 'A' || fromFile > 'H' ||
                toFile < 'A' || toFile > 'H')
                return null;

            return new Move(
                new BoardPoint(fromRank, fromFile - 'A' + 1),
                new BoardPoint(toRank, toFile - 'A' + 1)
                );
        }

        public Move(BoardPoint from, BoardPoint to)
        {
            this.From = from;
            this.To = to;
        }

        public BoardPoint From { get; }

        public BoardPoint To { get; }
    }
}
