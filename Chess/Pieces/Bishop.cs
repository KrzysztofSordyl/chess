﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Pieces
{
    public class Bishop : Piece
    {
        public Bishop(Color color) :
            base(PieceType.Bishop, color)
        {
        }

        public override int Value => 3;

        private LinkedList<Capture> moves = new LinkedList<Capture>();
        public override IEnumerable<Capture> Moves => this.moves;

        private LinkedList<Capture> captures = new LinkedList<Capture>();
        public override IEnumerable<Capture> Captures => this.captures;

        protected override void SetMovesAndCaptures()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
            this.moves.Clear();
            this.captures.Clear();

            var mc = Bishop.GetMovesAndCaptures(this.Field);
            foreach (var c in mc.Moves)
            {
                this.moves.AddLast(c);
                c.Field.PieceSet += ObservedFieldPieceSet;
                c.Field.PieceRemoved += ObservedFieldPieceRemoved;
            }
            foreach (var c in mc.Captures)
            {
                this.captures.AddLast(c);
            }
        }

        private void ObservedFieldPieceRemoved(Field source, Piece piece)
        {
            removeBlockingFromCapture(piece, this.moves);
            removeBlockingFromCapture(piece, this.captures);

            void removeBlockingFromCapture(Piece p, IEnumerable<Capture> captures)
            {
                foreach (var c in captures)
                {
                    if (c.Blockings.Contains(p))
                        c.Blockings.Remove(p);
                }
            }
        }

        private void ObservedFieldPieceSet(Field source, Piece piece)
        {
            var relativeSource = source.Position - this.Field.Position;

            foreach (var c in this.moves)
            {
                var relativeCapture = c.Field.Position - this.Field.Position;
                if (Math.Sign(relativeSource.Rank) == Math.Sign(relativeCapture.Rank) &&
                    Math.Sign(relativeSource.File) == Math.Sign(relativeCapture.File) &&
                    Math.Abs(relativeCapture.Rank) >= Math.Abs(relativeSource.Rank))
                    c.Blockings.AddLast(piece);
            }
            foreach (var c in this.captures)
            {
                var relativeCapture = c.Field.Position - this.Field.Position;
                if (Math.Sign(relativeSource.Rank) == Math.Sign(relativeCapture.Rank) &&
                    Math.Sign(relativeSource.File) == Math.Sign(relativeCapture.File) &&
                    Math.Abs(relativeCapture.Rank) > Math.Abs(relativeSource.Rank))
                    c.Blockings.AddLast(piece);
            }
        }

        public static (IEnumerable<Capture> Moves, IEnumerable<Capture> Captures) GetMovesAndCaptures(Field field)
        {
            var moves = new List<Capture>(13);
            var captures = new List<Capture>(13);

            processFields(field, 1, 1, new List<Piece>(7));
            processFields(field, -1, 1, new List<Piece>(7));
            processFields(field, 1, -1, new List<Piece>(7));
            processFields(field, -1, -1, new List<Piece>(7));

            return (moves, captures);

            void processFields(Field origin, int rankDirection, int fileDirection, List<Piece> blockings)
            {
                if (!fieldIsValid(origin.Rank + rankDirection, origin.File + fileDirection))
                    return;

                var next = origin.OnDirection(rankDirection, fileDirection);
                captures.Add(new Capture(next, blockings));
                if (next.Piece != null)
                    blockings.Add(next.Piece);
                moves.Add(new Capture(next, blockings));

                processFields(next, rankDirection, fileDirection, blockings);
            }
            bool fieldIsValid(int rank, int file) =>
                rank <= 8 && rank >= 1 && file <= 8 && file >= 1;
        }

        public override void Dispose()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
        }
    }
}
