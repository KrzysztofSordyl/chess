﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Pieces
{
    public class King : Piece
    {
        public King(Color color) :
            base(PieceType.King, color)
        {
        }

        public override int Value => int.MaxValue;

        private List<Capture> moves = new List<Capture>(8);
        public override IEnumerable<Capture> Moves => this.moves;

        private List<Capture> captures = new List<Capture>(8);
        public override IEnumerable<Capture> Captures => this.captures;

        protected override void SetMovesAndCaptures()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
            var f = this.Field;
            this.moves.Clear();
            this.captures.Clear();

            if (f.Up != null)
                processField(f.Up);
            if (f.Down != null)
                processField(f.Down);
            if (f.Left != null)
                processField(f.Left);
            if (f.Right != null)
                processField(f.Right);
            if (f.Up != null && f.Right != null)
                processField(f.Up.Right);
            if (f.Right != null && f.Down != null)
                processField(f.Right.Down);
            if (f.Down != null && f.Left != null)
                processField(f.Down.Left);
            if (f.Left != null && f.Up != null)
                processField(f.Left.Up);

            void processField(Field field)
            {
                //switch (this.Color)
                //{
                //    case Color.White:
                //        if (field.ControlledByBlack
                //            .Where(c => c.Blockings.Count == 0)
                //            .Count() != 0)
                //            return;
                //        break;
                //    case Color.Black:
                //        if (field.ControlledByWhite
                //            .Where(c => c.Blockings.Count == 0)
                //            .Count() != 0)
                //            return;
                //        break;
                //}
                field.PieceSet += ObservedFieldPieceSet;
                field.PieceRemoved += ObservedFieldPieceRemoved;
                this.captures.Add(new Capture(field));
                var blockings = new List<Piece>(1);
                if (field.Piece != null)
                    blockings.Add(field.Piece);
                this.moves.Add(new Capture(field, blockings));
            }
        }

        private void ObservedFieldPieceRemoved(Field source, Piece piece)
        {
            foreach (var c in this.moves)
            {
                if (c.Field == source)
                    c.Blockings.Remove(piece);
            }
        }

        private void ObservedFieldPieceSet(Field source, Piece piece)
        {
            foreach (var c in this.moves)
            {
                if (c.Field == source)
                    c.Blockings.AddLast(piece);
            }
        }

        public override void Dispose()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
        }
    }
}
