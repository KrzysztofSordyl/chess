﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Pieces
{
    public class Knight : Piece
    {
        public Knight(Color color) 
            : base(PieceType.Knight, color)
        {
        }

        public override int Value => 3;

        private List<Capture> moves = new List<Capture>(8);
        public override IEnumerable<Capture> Moves => this.moves;

        private List<Capture> captures = new List<Capture>(8);
        public override IEnumerable<Capture> Captures => this.captures;

        protected override void SetMovesAndCaptures()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
            var f = this.Field;
            this.moves.Clear();
            this.captures.Clear();

            if (f.Up?.Up != null)
            {
                if (f.Up.Up.Left != null)
                    processField(f.Up.Up.Left);
                if (f.Up.Up.Right != null)
                    processField(f.Up.Up.Right);
            }
            if (f.Down?.Down != null)
            {
                if (f.Down.Down.Left != null)
                    processField(f.Down.Down.Left);
                if (f.Down.Down.Right != null)
                    processField(f.Down.Down.Right);
            }
            if (f.Left?.Left != null)
            {
                if (f.Left.Left.Up != null)
                    processField(f.Left.Left.Up);
                if (f.Left.Left.Down != null)
                    processField(f.Left.Left.Down);
            }
            if (f.Right?.Right != null)
            {
                if (f.Right.Right.Up != null)
                    processField(f.Right.Right.Up);
                if (f.Right.Right.Down != null)
                    processField(f.Right.Right.Down);
            }

            void processField(Field field)
            {
                field.PieceSet += ObservedFieldPieceSet;
                field.PieceRemoved += ObservedFieldPieceRemoved;
                this.captures.Add(new Capture(field));
                var blockings = new List<Piece>(1);
                if (field.Piece != null)
                    blockings.Add(field.Piece);
                this.moves.Add(new Capture(field, blockings));
            }
        }

        private void ObservedFieldPieceRemoved(Field source, Piece piece)
        {
            foreach (var c in this.moves)
            {
                if (c.Field == source)
                    c.Blockings.Remove(piece);
            }
        }

        private void ObservedFieldPieceSet(Field source, Piece piece)
        {
            foreach (var c in this.moves)
            {
                if (c.Field == source)
                    c.Blockings.AddLast(piece);
            }
        }

        public override void Dispose()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
        }
    }
}
