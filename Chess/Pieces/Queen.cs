﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Pieces
{
    public class Queen : Piece
    {
        public Queen(Color color) :
            base(PieceType.Queen, color)
        {
        }

        public override int Value => 9;

        private LinkedList<Capture> moves = new LinkedList<Capture>();
        public override IEnumerable<Capture> Moves => this.moves;

        private LinkedList<Capture> captures = new LinkedList<Capture>();
        public override IEnumerable<Capture> Captures => this.captures;

        protected override void SetMovesAndCaptures()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
            var b = Bishop.GetMovesAndCaptures(this.Field);
            var r = Rook.GetMovesAndCaptures(this.Field);

            this.moves.Clear();
            foreach (var c in b.Moves.Concat(r.Moves))
            {
                this.moves.AddLast(c);
                c.Field.PieceSet += ObservedFieldPieceSet;
                c.Field.PieceRemoved += ObservedFieldPieceRemoved;
            }
            this.captures.Clear();
            foreach (var c in b.Captures.Concat(r.Captures))
            {
                this.captures.AddLast(c);
            }
        }

        private void ObservedFieldPieceRemoved(Field source, Piece piece)
        {
            removeBlockingFromCapture(piece, this.moves);
            removeBlockingFromCapture(piece, this.captures);

            void removeBlockingFromCapture(Piece p, IEnumerable<Capture> captures)
            {
                foreach (var c in captures)
                {
                    if (c.Blockings.Contains(p))
                        c.Blockings.Remove(p);
                }
            }
        }

        private void ObservedFieldPieceSet(Field source, Piece piece)
        {
            var relativeSource = source.Position - this.Field.Position;
            bool bishop = Math.Abs(relativeSource.Rank) == Math.Abs(relativeSource.File);

            foreach (var c in this.moves)
            {
                var relativeCapture = c.Field.Position - this.Field.Position;
                bool captureBishop = Math.Abs(relativeCapture.Rank) == Math.Abs(relativeCapture.File);
                if (!bishop && !captureBishop)
                {
                    if (Math.Sign(relativeSource.Rank) == Math.Sign(relativeCapture.Rank) &&
                        Math.Sign(relativeSource.File) == Math.Sign(relativeCapture.File))
                    {
                        if (relativeCapture.Rank == 0)
                        {
                            if (Math.Abs(relativeCapture.File) >= Math.Abs(relativeSource.File))
                                c.Blockings.AddLast(piece);
                        }
                        else
                        {
                            if (Math.Abs(relativeCapture.Rank) >= Math.Abs(relativeSource.Rank))
                                c.Blockings.AddLast(piece);
                        }
                    }
                }
                if (bishop && captureBishop)
                {
                    if (Math.Sign(relativeSource.Rank) == Math.Sign(relativeCapture.Rank) &&
                        Math.Sign(relativeSource.File) == Math.Sign(relativeCapture.File) &&
                        Math.Abs(relativeCapture.Rank) >= Math.Abs(relativeSource.Rank))
                        c.Blockings.AddLast(piece);
                }
            }

            foreach (var c in this.captures)
            {
                var relativeCapture = c.Field.Position - this.Field.Position;
                bool captureBishop = Math.Abs(relativeCapture.Rank) == Math.Abs(relativeCapture.File);

                if (!bishop && !captureBishop)
                {
                    if (Math.Sign(relativeSource.Rank) == Math.Sign(relativeCapture.Rank) &&
                    Math.Sign(relativeSource.File) == Math.Sign(relativeCapture.File))
                    {
                        if (relativeCapture.Rank == 0)
                        {
                            if (Math.Abs(relativeCapture.File) > Math.Abs(relativeSource.File))
                                c.Blockings.AddLast(piece);
                        }
                        else
                        {
                            if (Math.Abs(relativeCapture.Rank) > Math.Abs(relativeSource.Rank))
                                c.Blockings.AddLast(piece);
                        }
                    }
                }
                if (bishop && captureBishop)
                {
                    if (Math.Sign(relativeSource.Rank) == Math.Sign(relativeCapture.Rank) &&
                        Math.Sign(relativeSource.File) == Math.Sign(relativeCapture.File) &&
                        Math.Abs(relativeCapture.Rank) > Math.Abs(relativeSource.Rank))
                        c.Blockings.AddLast(piece);
                }
            }
        }

        public override void Dispose()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
        }
    }
}
