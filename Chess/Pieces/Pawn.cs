﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Pieces
{
    public class Pawn : Piece
    {
        public Pawn(Color color)
            : base(PieceType.Pawn, color)
        {
        }

        public override int Value => 1;

        private List<Capture> moves = new List<Capture>(2);
        public override IEnumerable<Capture> Moves => this.moves;

        private List<Capture> captures = new List<Capture>(2);
        public override IEnumerable<Capture> Captures => this.captures;

        protected override void SetMovesAndCaptures()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
            var f = this.Field;
            #region moves
            this.moves.Clear();
            if (this.Color == Color.White)
            {
                var blockings = new LinkedList<Piece>();
                if (f.Up.Piece != null)
                    blockings.AddLast(f.Up.Piece);
                this.moves.Add(new Capture(f.Up, blockings));
                if (f.Rank == 2)
                {
                    if (f.Up.Up.Piece != null)
                        blockings.AddLast(f.Up.Up.Piece);
                    this.moves.Add(new Capture(f.Up.Up, blockings));
                }
            }
            else
            {
                var blockings = new LinkedList<Piece>();
                if (f.Down.Piece != null)
                    blockings.AddLast(f.Down.Piece);
                this.moves.Add(new Capture(f.Down, blockings));
                if (f.Rank == 7)
                {
                    if (f.Down.Down.Piece != null)
                        blockings.AddLast(f.Down.Down.Piece);
                    this.moves.Add(new Capture(f.Down.Down, blockings));
                }
            }
            #endregion
            #region captures
            this.captures.Clear();
            if (this.Color == Color.White)
            {
                if (f.Left != null)
                    this.captures.Add(new Capture(f.Left.Up));
                if (f.Right != null)
                    this.captures.Add(new Capture(f.Right.Up));
            }
            else
            {
                if (f.Left != null)
                    this.captures.Add(new Capture(f.Left.Down));
                if (f.Right != null)
                    this.captures.Add(new Capture(f.Right.Down));
            }
            #endregion
            foreach (var c in this.moves)
            {
                c.Field.PieceSet += ObservedFieldPieceSet;
                c.Field.PieceRemoved += ObservedFieldPieceRemoved;
            }
        }

        private void ObservedFieldPieceRemoved(Field source, Piece piece)
        {
            foreach (var c in this.moves)
            {
                if (c.Blockings.Contains(piece))
                    c.Blockings.Remove(piece);
            }
        }

        private void ObservedFieldPieceSet(Field source, Piece piece)
        {
            if (this.moves.Count == 1)
                this.moves.Single().Blockings.AddLast(piece);
            else
            {
                int distance = Math.Abs((this.Field.Position - source.Position).Rank);
                if (distance == 2)
                    this.moves.Single(c => c.Field == source).Blockings.AddLast(piece);
                else
                    foreach (var c in this.moves)
                    {
                        c.Blockings.AddLast(piece);
                    }
            }
        }

        public override void Dispose()
        {
            foreach (var c in this.moves)
            {
                c.Field.PieceSet -= ObservedFieldPieceSet;
                c.Field.PieceRemoved -= ObservedFieldPieceRemoved;
            }
        }

        public void EnableInPassing(Field field, Piece piece)
        {
            this.captures.Add(new Capture(field, piece));
        }
        public void DisableInPassing(Field field, Piece piece)
        {
            this.captures.RemoveAll(c => c.Field == field && c.Piece == piece);
        }
    }
}