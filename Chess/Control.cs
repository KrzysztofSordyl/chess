﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Control
    {
        public Control(Piece piece, IEnumerable<Piece> blockings)
        {
            this.Piece = piece;
            foreach (var b in blockings)
            {
                this.Blockings.AddLast(b);
            }
        }

        public Piece Piece { get; }

        public LinkedList<Piece> Blockings { get; } = new LinkedList<Piece>();
    }
}
