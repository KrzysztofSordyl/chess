﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Field
    {
        public Field(int rank, int file, Board board, Color color, Field up, Field down, Field left, Field right)
        {
            this.Rank = rank;
            this.File = file;
            this.Board = board;
            this.Color = color;
            this.Up = up;
            this.Down = down;
            this.Left = left;
            this.Right = right;
        }

        public Color Color { get; }

        public int Rank { get; }
        public int File { get; }
        public BoardPoint Position => new BoardPoint(this.Rank, this.File);

        public Board Board { get; }

        private Piece piece;
        public Piece Piece
        {
            get => this.piece;
            set
            {
                if (this.piece != value)
                {
                    Piece old = this.piece;
                    this.piece = value;
                    if (old != null)
                        old.Field = null;
                    if (value != null && value.Field != this)
                        value.Field = this;
                    if (old != null)
                        this.PieceRemoved?.Invoke(this, old);
                    if (value != null)
                        this.PieceSet?.Invoke(this, value);
                }
            }
        }

        public event PieceChangedEventHandler PieceSet;
        public event PieceChangedEventHandler PieceRemoved;

        private Field up;
        private Field down;
        private Field left;
        private Field right;

        public Field Up
        {
            get => this.up;
            set
            {
                if (this.up != null)
                    this.up.Down = null;
                this.up = value;
                if (value != null && value.Down != this)
                    value.Down = this;
            }
        }
        public Field Down
        {
            get => this.down;
            set
            {
                if (this.down != null)
                    this.down.Up = null;
                this.down = value;
                if (value != null && value.Up != this)
                    value.Up = this;
            }
        }
        public Field Left
        {
            get => this.left;
            set
            {
                if (this.left != null)
                    this.left.Right = null;
                this.left = value;
                if (value != null && value.Right != this)
                    value.Right = this;
            }
        }
        public Field Right
        {
            get => this.right;
            set
            {
                if (this.right != null)
                    this.right.Left = null;
                this.right = value;
                if (value != null && value.Left != this)
                    value.Left = this;
            }
        }

        public Field OnDirection(int rankShift, int fileShift)
        {
            Field result = this;
            
            if (rankShift > 0)
            {
                result = result.Up;
                result = result.OnDirection(rankShift - 1, 0);
            }
            if (rankShift < 0)
            {
                result = result.Down;
                result = result.OnDirection(rankShift + 1, 0);
            }

            if (fileShift > 0)
            {
                result = result.Right;
                result = result.OnDirection(fileShift - 1, 0);
            }
            if (fileShift < 0)
            {
                result = result.Left;
                result = result.OnDirection(fileShift + 1, 0);
            }

            return result;
        }

        public List<Control> ControlledBy { get; } = new List<Control>();
        public IEnumerable<Control> ControlledByWhite => this.ControlledBy.Where(c => c.Piece.Color == Color.White);
        public IEnumerable<Control> ControlledByBlack => this.ControlledBy.Where(c => c.Piece.Color == Color.Black);
    }

    public delegate void PieceChangedEventHandler(Field source, Piece piece);
}
