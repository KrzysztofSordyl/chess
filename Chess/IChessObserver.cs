﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public interface IChessObserver
    {
        void MakeMove(Move move);

        void Check(bool mate);

        void Stalemate();
    }
}
