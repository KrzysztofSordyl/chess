﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Capture
    {
        public Capture(Field field)
        {
            this.Field = field;
        }
        public Capture(Field field, IEnumerable<Piece> blockings)
            : this(field)
        {
            foreach (var b in blockings)
            {
                this.Blockings.AddLast(b);
            }
        }
        public Capture(Field field, Piece piece)
            : this(field)
        {
            this.piece = piece;
        }

        private Piece piece;
        public Piece Piece
            => this.piece ?? this.Field.Piece;

        public Field Field { get; }

        public LinkedList<Piece> Blockings { get; } = new LinkedList<Piece>();
    }
}
