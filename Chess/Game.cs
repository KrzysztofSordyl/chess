﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Game
    {
        public Board Board { get; set; }

        private Dictionary<Color, IChessPlayer> players { get; }
        private List<IChessObserver> observers { get; }
        
        public Game(IChessPlayer white, IChessPlayer black, IEnumerable<IChessObserver> observers = null)
        {
            if (white == null)
                throw new ArgumentNullException(nameof(white));
            if (black == null)
                throw new ArgumentNullException(nameof(black));

            this.players = new Dictionary<Color, IChessPlayer>()
            {
                { Color.White, white },
                { Color.Black, black }
            };
            white.ChooseMove += whiteChooseMove;
            black.ChooseMove += blackChooseMove;

            this.observers = new List<IChessObserver>(this.players.Values);
            if (observers != null)
                this.observers.AddRange(observers);
        }

        private void blackChooseMove(Move move)
            => this.makeMove(move, Color.Black);
        private void whiteChooseMove(Move move)
            => this.makeMove(move, Color.White);

        private readonly object makeMoveLock = new object();
        private void makeMove(Move move, Color player)
        {
            if (!this.IsRunning)
                return;
            lock (this.makeMoveLock)
            {
                if (this.OnMove != player)
                    return;
                else
                {
                    bool check = false;
                    bool mate = false;
                    try
                    {
                        var (Check, Mate) = this.Board.MakeMove(move);
                        check = Check;
                        mate = Mate;
                    }
                    catch (InvalidOperationException e)
                    {
                        this.players[this.OnMove].ThrowException(e);
                        return;
                    }
                    foreach (var o in this.observers)
                    {
                        o.MakeMove(move);
                    }
                    if (check)
                    {
                        foreach (var o in this.observers)
                        {
                            o.Check(mate);
                        }
                    }
                    switch (this.OnMove)
                    {
                        case Color.White:
                            this.OnMove = Color.Black;
                            break;
                        case Color.Black:
                            this.OnMove = Color.White;
                            break;
                    }
                }
            }
        }

        public ChessMoveStorage Moves { get; private set; }

        public Color OnMove { get; private set; }

        public bool IsRunning { get; private set; } = false;

        public void Run()
        {
            this.IsRunning = false;
            this.Board?.Dispose();
            this.Board = Board.Create();
            this.run(Color.White);
        }
        public void Run(ChessMoveStorage game)
        {
            this.IsRunning = false;
            this.Board?.Dispose();
            this.Board = Board.Load(game);
            this.run(game.CurrentPlayer);
        }
        private void run(Color onMove)
        {
            this.OnMove = onMove;
            this.Board.WhitePromotion += this.players[Color.White].SelectPromotion;
            this.Board.BlackPromotion += this.players[Color.Black].SelectPromotion;
            this.IsRunning = true;
        }
    }
}