﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public interface IChessPlayer : IChessObserver
    {
        event MoveEventHandler ChooseMove;

        void ThrowException(Exception exception);

        PieceType SelectPromotion();
    }

    public delegate void MoveEventHandler(Move move);
}
