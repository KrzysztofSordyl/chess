﻿using Chess.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public abstract class Piece : IDisposable
    {
        public static Piece Create(PieceType type, Color color)
        {
            switch (type)
            {
                case PieceType.King: return new King(color);
                case PieceType.Queen: return new Queen(color);
                case PieceType.Rook: return new Rook(color);
                case PieceType.Bishop: return new Bishop(color);
                case PieceType.Knight: return new Knight(color);
                case PieceType.Pawn: return new Pawn(color);
            }
            return null;
        }

        private Field field;

        public Piece(PieceType type, Color color)
        {
            this.Type = type;
            this.Color = color;
        }

        public PieceType Type { get; }

        public Color Color { get; }

        public abstract int Value { get; }

        public Field Field
        {
            get => this.field;
            set
            {
                if (this.field != value)
                {
                    foreach (var c in this.Captures)
                    {
                        c.Field.ControlledBy.RemoveAll(x => x.Piece == this);
                    }

                    Field old = this.field;
                    this.field = value;
                    if (old != null)
                        old.Piece = null;
                    if (value != null && value.Piece != this)
                        value.Piece = this;
                    if (value != null)
                    {
                        this.SetMovesAndCaptures();
                        foreach (var c in this.Captures)
                        {
                            c.Field.ControlledBy.Add(new Control(this, c.Blockings));
                        }
                    }
                }
            }
        }

        public abstract IEnumerable<Capture> Moves { get; }

        public abstract IEnumerable<Capture> Captures { get; }

        protected abstract void SetMovesAndCaptures();

        public abstract void Dispose();
    }

    public enum PieceType
    {
        King,
        Queen,
        Rook,
        Bishop,
        Knight,
        Pawn
    }

    public enum Color
    {
        White,
        Black
    }
}
