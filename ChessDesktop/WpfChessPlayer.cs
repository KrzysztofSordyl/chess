﻿using Chess;
using ChessDesktop.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ChessDesktop
{
    public class WpfChessPlayer : IChessPlayer
    {
        public WpfChessPlayer(Color color)
        {
            this.Color = color;
        }

        public Color Color { get; }

        public event MoveEventHandler ChooseMove;

        public void PickMove(Move move)
        {
            this.ChooseMove(move);
        }

        public void Check(bool mate)
        {
            if (mate)
                MessageBox.Show("Checkmate!");
            else
                MessageBox.Show("Check!");
        }

        public void MakeMove(Move move)
        {
            
        }

        public void ThrowException(Exception exception)
        {
            MessageBox.Show(exception.Message);
        }

        public PieceType SelectPromotion()
        {
            var pw = new PromotionWindow(this.Color);
            pw.ShowDialog();
            return pw.SelectedPiece;
        }

        public void Stalemate()
        {
            MessageBox.Show("Stalemate!");
        }
    }
}
