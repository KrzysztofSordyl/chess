﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ChessDesktop.Converters
{
    public class BooleansToHighLightMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var booleans = values.Cast<bool>()
                .ToList();
            if (booleans[0]) return Brushes.DeepSkyBlue;
            if (booleans[1]) return Brushes.Red;
            if (booleans[2]) return Brushes.ForestGreen;
            return Brushes.Transparent;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
