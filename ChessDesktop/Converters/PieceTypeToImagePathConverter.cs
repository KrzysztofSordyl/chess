﻿using Chess;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ChessDesktop.Converters
{
    public class PieceTypeAndColorToImagePathConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var images = parameter as string[];
            return images[0];
            int index = 0;
            switch ((PieceType?)values[0])
            {
                case PieceType.King:
                    index = 1;
                    break;
                case PieceType.Queen:
                    index = 4;
                    break;
                case PieceType.Rook:
                    index = 5;
                    break;
                case PieceType.Bishop:
                    index = 0;
                    break;
                case PieceType.Knight:
                    index = 2;
                    break;
                case PieceType.Pawn:
                    index = 3;
                    break;
                default:
                    return null;
            }
            if ((Color)values[1] == Color.White)
                index += 6;
            return images[index];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}