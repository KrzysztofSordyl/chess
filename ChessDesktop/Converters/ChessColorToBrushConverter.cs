﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ChessDesktop.Converters
{
    [ValueConversion(typeof(Chess.Color), typeof(SolidColorBrush))]
    public class ChessColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Chess.Color)value == Chess.Color.Black
                ? Brushes.SaddleBrown
                : Brushes.SandyBrown;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
