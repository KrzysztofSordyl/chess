﻿using Chess;
using ChessDesktop.ViewModels;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessDesktop.Windows
{
    public partial class PromotionWindow : Window
    {
        public static double TopPosition;
        public static double LeftPosition;

        private PromotionViewModel viewModel;

        public PromotionWindow(Color color)
        {
            InitializeComponent();
            this.viewModel = new PromotionViewModel(color);
            this.DataContext = this.viewModel;

            var dpiMatrix = PresentationSource.FromVisual(Application.Current.MainWindow).CompositionTarget.TransformToDevice;
            this.Top = TopPosition / dpiMatrix.M22;
            this.Left = LeftPosition / dpiMatrix.M11;
        }

        public PieceType SelectedPiece => this.viewModel.SelectedPiece.Type;

        private void ListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space && this.viewModel.SelectedPiece != null)
                this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ((ListBoxItem)this.mainListBox.ItemContainerGenerator.ContainerFromItem(this.mainListBox.SelectedValue)).Focus();
        }
    }
}
