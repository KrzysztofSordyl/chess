﻿using Chess;
using ChessDesktop.ViewModels;
using ChessDesktop.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessDesktop.Views
{
    /// <summary>
    /// Interaction logic for BoardView.xaml
    /// </summary>
    public partial class BoardView : UserControl
    {
        public BoardView()
        {
            InitializeComponent();
        }
        
        private BoardViewModel viewModel => this.DataContext as BoardViewModel;

        private void ListBoxItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                var point = (sender as ListBoxItem).PointToScreen(new Point(0, 0));
                PromotionWindow.TopPosition = point.Y;
                PromotionWindow.LeftPosition = point.X;

                var field = ((sender as ListBoxItem).Content as FieldViewModel);
                this.viewModel.SpacePushed(field);
            }
        }

        private void Flip_Click(object sender, RoutedEventArgs e)
        {
            this.viewModel.Flip();
        }
    }
}
