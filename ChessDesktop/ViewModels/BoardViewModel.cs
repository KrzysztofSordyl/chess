﻿using Chess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessDesktop.ViewModels
{
    public class BoardViewModel : INotifyPropertyChanged
    {
        public BoardViewModel(Game game)
        {
            this.game = game;
            this.board = game.Board;
            this.board.AdditionalFieldChanged += board_AdditionalFieldChanged;

            this.Fields = new ObservableCollection<FieldViewModel>(getFields());
            IEnumerable<FieldViewModel> getFields()
            {
                for (int rank = 8; rank >= 1; rank--)
                {
                    for (int file = 1; file <= 8; file++)
                    {
                        yield return new FieldViewModel(this.board[rank, file]);
                    }
                }
            }
        }

        private void board_AdditionalFieldChanged(Field source, Piece piece)
        {
            foreach (var f in this.Fields)
            {
                if (f.Is(source))
                    f.RaisePropertyChanged("Piece");
            }
        }

        private readonly Game game;
        private readonly Board board;

        public ObservableCollection<FieldViewModel> Fields { get; private set; }

        private FieldViewModel selectedField;
        public FieldViewModel SelectedField
        {
            set
            {
                if (this.selectedField != value)
                {
                    if (this.selectedField != null)
                        this.selectedField.IsSelected = false;
                    this.selectedField = value;
                    if (value != null)
                        value.IsSelected = true;
                }
            }
        }

        private void propertyChanged(string property)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private List<FieldViewModel> availableAttacks { get; } = new List<FieldViewModel>();
        private List<FieldViewModel> availableMoves { get; } = new List<FieldViewModel>();
        private FieldViewModel hardSelectedField;
        public FieldViewModel HardSelectedField
        {
            get => this.hardSelectedField;
            set
            {
                if (this.hardSelectedField != value)
                {
                    if (this.hardSelectedField != null)
                    {
                        this.hardSelectedField.IsHardSelected = false;
                        this.availableMoves.ForEach(m => m.IsMoveAvailable = false);
                        this.availableMoves.Clear();
                        this.availableAttacks.ForEach(m => m.IsAttackAvailable = false);
                        this.availableAttacks.Clear();
                    }
                    this.hardSelectedField = value;
                    if (value != null && value.Piece != null && this.board[value.Position].Piece.Color == this.game.OnMove)
                    {
                        value.IsHardSelected = true;
                        IEnumerable<Field> moves = value.Moves;
                        IEnumerable<Field> captures = value.Captures;
                        foreach (var f in this.Fields)
                        {
                            if (moves.Any(m => f.Is(m)))
                            {
                                f.IsMoveAvailable = true;
                                this.availableMoves.Add(f);
                            }
                            if (captures.Any(m => f.Is(m)))
                            {
                                f.IsAttackAvailable = true;
                                this.availableAttacks.Add(f);
                            }
                        }
                    }
                }
            }
        }

        public event MoveEventHandler WhiteMakeMove;
        public event MoveEventHandler BlackMakeMove;

        public IEnumerable<int> Ranks
            => this.flipped
            ? Enumerable.Range(1, 8)
            : Enumerable.Range(1, 8).Reverse();
        public IEnumerable<char> Files
            => this.flipped
            ? Enumerable.Range(0, 8).Select(x => (char)(x + 'A')).Reverse()
            : Enumerable.Range(0, 8).Select(x => (char)(x + 'A'));

        private bool flipped = false;
        public void Flip()
        {
            this.flipped = !this.flipped;
            this.Fields = new ObservableCollection<FieldViewModel>(this.Fields.Reverse());
            this.propertyChanged("Fields");
            this.propertyChanged("Ranks");
            this.propertyChanged("Files");
        }

        public void SpacePushed(FieldViewModel field)
        {
            if (this.availableMoves.Contains(field) ||
                this.availableAttacks.Contains(field))
            {
                var move = new Move(this.hardSelectedField.Position, field.Position);
                switch (this.board[this.hardSelectedField.Position].Piece.Color)
                {
                    case Color.White:
                        this.WhiteMakeMove(move);
                        break;
                    case Color.Black:
                        this.BlackMakeMove(move);
                        break;
                }
                this.hardSelectedField.RaisePropertyChanged("Piece");
                field.RaisePropertyChanged("Piece");
                this.HardSelectedField = null;
            }
            else
                this.HardSelectedField = field;
        }
    }
}
