﻿using Chess;
using System.Collections.Generic;
using System.Linq;

namespace ChessDesktop.ViewModels
{
    public class PromotionViewModel
    {
        public PromotionViewModel(Color color)
        {
            this.Pieces = new List<PromotionFieldViewModel>()
            {
                (PieceType.Bishop, color),
                (PieceType.Knight, color),
                (PieceType.Rook, color),
                (PieceType.Queen, color)
            };

            this.SelectedPiece = this.Pieces
                .Single(p => p.Type == PieceType.Queen);
        }

        public List<PromotionFieldViewModel> Pieces { get; }

        private PromotionFieldViewModel selectedPiece;
        public PromotionFieldViewModel SelectedPiece
        {
            get => this.selectedPiece;
            set
            {
                if (this.selectedPiece != null)
                    this.selectedPiece.IsSelected = false;
                if (value != null)
                    value.IsSelected = true;
                this.selectedPiece = value;
            }
        }
    }
}
