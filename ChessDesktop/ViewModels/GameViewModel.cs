﻿using Chess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessDesktop.ViewModels
{
    public class GameViewModel
    {
        private WpfChessPlayer white;
        private WpfChessPlayer black;

        private readonly Game game;

        public GameViewModel()
        {
            this.white = new WpfChessPlayer(Color.White);
            this.black = new WpfChessPlayer(Color.Black);
            this.game = new Game(white, black);
            this.game.Run();
            this.Board = new BoardViewModel(this.game);
            this.Board.WhiteMakeMove += (move) => this.white.PickMove(move);
            this.Board.BlackMakeMove += (move) => this.black.PickMove(move);
        }

        public BoardViewModel Board { get; }
    }
}
