﻿using Chess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ChessDesktop.ViewModels
{
    public class FieldViewModel : INotifyPropertyChanged
    {
        static FieldViewModel()
        {
            Images = new Dictionary<(PieceType, Color), BitmapImage>();
            foreach (var p in Enum.GetValues(typeof(PieceType)) as PieceType[])
            {
                foreach (var c in Enum.GetValues(typeof(Color)) as Color[])
                {
                    Images.Add(
                        (p, c), new BitmapImage(
                            new Uri(@"\Images\" + c + p + ".png", UriKind.Relative)));
                }
            }
        }

        private readonly Field field;

        public FieldViewModel(Field field)
        {
            this.field = field;
        }

        public Color Color => this.field.Color;

        private bool isSelected = false;
        public bool IsSelected
        {
            get => this.isSelected;
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged("IsSelected");
            }
        }

        private bool isHardSelected = false;
        public bool IsHardSelected
        {
            get => this.isHardSelected;
            set
            {
                this.isHardSelected = value;
                this.RaisePropertyChanged("IsHardSelected");
            }
        }

        private bool isAttackAvailable = false;
        public bool IsAttackAvailable
        {
            get => this.isAttackAvailable;
            set
            {
                this.isAttackAvailable = value;
                this.RaisePropertyChanged("IsAttackAvailable");
            }
        }

        private bool isMoveAvailable = false;
        public bool IsMoveAvailable
        {
            get => this.isMoveAvailable;
            set
            {
                this.isMoveAvailable = value;
                this.RaisePropertyChanged("IsMoveAvailable");
            }
        }

        public static Dictionary<(PieceType, Color), BitmapImage> Images { get; }
        public System.Windows.Media.ImageSource Piece
        {
            get
            {
                if (this.field.Piece == null)
                    return null;
                return Images[(this.field.Piece.Type, this.field.Piece.Color)];
            }
        }

        public void RaisePropertyChanged(string property)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public IEnumerable<Field> Moves =>
            this.field.Piece?.Moves
            .Where(m => m.Blockings.Count == 0)
            .Select(m => m.Field) ?? new List<Field>(0);

        public IEnumerable<Field> Captures =>
            this.field.Piece?.Captures
            .Where(m => m.Blockings.Count == 0 && m.Piece != null && m.Piece.Color != this.field.Piece.Color)
            .Select(m => m.Field) ?? new List<Field>(0);

        public bool Is(Field field) => this.field == field;

        public BoardPoint Position
            => this.field.Position;
    }
}
