﻿using Chess;
using System.ComponentModel;

namespace ChessDesktop.ViewModels
{
    public class PromotionFieldViewModel : INotifyPropertyChanged
    {
        public PieceType Type { get; }
        private Color color;

        public void RaisePropertyChanged(string property)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public PromotionFieldViewModel(PieceType type, Color color)
        {
            this.Type = type;
            this.color = color;
        }

        public Color Color => Color.Black;

        private bool isSelected = false;
        public bool IsSelected
        {
            get => this.isSelected;
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged("IsSelected");
            }
        }
        public bool IsHardSelected => false;
        public bool IsAttackAvailable => false;
        public bool IsMoveAvailable => false;

        public System.Windows.Media.ImageSource Piece
            => FieldViewModel.Images[(this.Type, this.color)];

        public static implicit operator PromotionFieldViewModel((PieceType Type, Color Color) piece)
        {
            return new PromotionFieldViewModel(piece.Type, piece.Color);
        }
    }
}
