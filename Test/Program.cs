﻿using Chess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program : IChessPlayer
    {
        static void Main(string[] args)
        {
            Game g = new Game(new Program(true), new Program());
            g.Run();
        }

        public Program(bool draw = false)
        {
            this.draw = draw;
            if (draw)
                this.drawBoard();
        }

        private bool draw;
        private Board board = Board.Create();

        private void drawBoard()
        {
            Console.Clear();
            for (int rank = 8; rank >= 1; rank--)
            {
                for (int file = 1; file <= 8; file++)
                {
                    Piece p = this.board[rank, file].Piece;
                    drawPiece(p);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            //this.board[8, 2].ControlledBy.ForEach(c =>
            // {
            //     drawPiece(c.Piece);
            //     Console.Write("\t");
            //     c.Blockings.ToList()
            //     .ForEach(b => drawPiece(b));
            //     Console.WriteLine();
            // });
            this.board[1, 1].Piece?.Moves
                .ToList()
                .ForEach(m =>
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(m.Field.Position + "\t");
                    m.Blockings
                    .ToList()
                    .ForEach(b => drawPiece(b));
                    Console.WriteLine();
                });
        }

        private static void drawPiece(Piece p)
        {
            if (p == null)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(".");
            }
            else
            {
                switch (p.Color)
                {
                    case Color.White:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        break;
                    case Color.Black:
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                }
                switch (p.Type)
                {
                    case PieceType.King:
                        Console.Write("K");
                        break;
                    case PieceType.Queen:
                        Console.Write("Q");
                        break;
                    case PieceType.Rook:
                        Console.Write("R");
                        break;
                    case PieceType.Bishop:
                        Console.Write("B");
                        break;
                    case PieceType.Knight:
                        Console.Write("J");
                        break;
                    case PieceType.Pawn:
                        Console.Write("P");
                        break;
                }
            }
        }

        public Move ChooseMove()
        {
            string s = "";
            Move move = null;
            while (move == null)
            {
                s = Console.ReadLine();
                move = parseMove(s);
            }
            return move;

            Move parseMove(string moveString)
            {
                moveString = moveString.Replace(" ", "");
                if (moveString.Length != 4)
                    return null;

                int fromRank;
                if (!int.TryParse(moveString.Substring(1, 1), out fromRank))
                    return null;
                int toRank;
                if (!int.TryParse(moveString.Substring(3, 1), out toRank))
                    return null;
                char fromFile = moveString[0];
                char toFile = moveString[2];

                if (fromRank < 1 || fromRank > 8 ||
                    toRank < 1 || toRank > 8 ||
                    fromFile < 'A' || fromFile > 'H' ||
                    toFile < 'A' || toFile > 'H')
                    return null;

                return new Move(
                    new BoardPoint(fromRank, fromFile - 'A' + 1),
                    new BoardPoint(toRank, toFile - 'A' + 1)
                    );
            }
        }

        public void MakeMove(Move move)
        {
            this.board.MakeMove(move);
            if (this.draw)
                this.drawBoard();
        }

        public void ThrowException(Exception exception)
        {
            Console.WriteLine(exception.Message);
        }
    }
}
